<?php

namespace Drupal\drd_migrate;

use Drupal\Component\Serialization\Json;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\drd\Entity\Domain;
use Drupal\drd\Entity\Host;
use Exception;

/**
 * Class Import.
 *
 * @package Drupal\drd
 */
class Import {

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Import constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(AccountProxyInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Manage output to console depending on context.
   *
   * @param \Drupal\Console\Core\Style\DrupalStyle|null $io
   *   The IO object which might be NULL.
   * @param string $text
   *   The text to be output.
   * @param bool $error
   *   Indicate if this is an error message or not.
   */
  private function output($io, $text, $error = FALSE) {
    if (isset($io)) {
      if ($error) {
        $io->error($text);
      }
      else {
        $io->info($text);
      }
    }
    else if ($error) {
      drush_set_error($text);
    }
    else {
      print($text);
    }
  }

  /**
   * Execute the import command.
   *
   * @param string $filename
   *   The full path and filename which holds the inventory for import.
   * @param \Drupal\Console\Core\Style\DrupalStyle $io
   *   The IO object from Drush or Console for output.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function execute($filename, DrupalStyle $io = NULL) {
    if (!file_exists($filename)) {
      $this->output($io, 'Inventory file does not exist!', TRUE);
      return;
    }
    try {
      $inventory = Json::decode(file_get_contents($filename));
    }
    catch (Exception $ex) {
      $this->output($io, 'Inventory file can not be read!', TRUE);
      return;
    }
    /** @var \Drupal\Core\Session\AccountInterface $account */
    $account = $this->entityTypeManager->getStorage('user')->load(1);
    $this->currentUser->setAccount($account);
    $storage = $this->entityTypeManager->getStorage('drd_core');

    foreach ($inventory as $id => $coredomains) {
      $this->output($io, 'Import core ' . $id);
      /** @var \Drupal\drd\Entity\Core $core */
      $core = $storage->create([
        'name' => 'Migrate ' . $id,
      ]);
      foreach ($coredomains as $coredomain) {
        $url = $coredomain['ssl'] ? 'https://' : 'http://';
        $url .= $coredomain['url'];
        $this->output($io, '  Import ' . $url);
        if ($domain = Domain::instanceFromUrl($core, $url, [])) {
          if ($domain->isNew()) {
            $domain->initValues($coredomain['url']);
          }
          else {
            $core = $domain->getCore();
          }
          if ($domain->pushOTT($coredomain['token'])) {
            if ($core->isNew()) {
              // Try to find the correct host or create a new one.
              $host = Host::findOrCreateByHost(parse_url($url, PHP_URL_HOST));
              $core->setHost($host);
              if (!$domain->initCore($core)) {
                continue;
              }
            }
            $domain->set('installed', 1);
            $domain->setCore($core);
            $domain->save();
            $core = $domain->getCore();
          }
        }
      }
    }
  }

}

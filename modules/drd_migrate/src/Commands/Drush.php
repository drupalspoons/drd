<?php

namespace Drupal\drd_migrate\Commands;

use Drupal\drd_migrate\Import;
use Drush\Commands\DrushCommands;

/**
 * Class Base.
 *
 * @package Drupal\drd
 */
class Drush extends DrushCommands {

  /**
   * @var \Drupal\drd_migrate\Import
   */
  protected $service;

  /**
   * Drush constructor.
   *
   * @param \Drupal\drd_migrate\Import $service
   */
  public function __construct(Import $service) {
    parent::__construct();
    $this->service = $service;
  }

  /**
   * Configure this domain for communication with a DRD instance.
   *
   * @param string $inventory
   *   Filename containing the json with you DRD 7 inventory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @command drd:migratefromd7
   * @aliases drd-migrate-from-d7
   */
  public function migrate($inventory) {
    $this->service->execute($inventory);
  }

}

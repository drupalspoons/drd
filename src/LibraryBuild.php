<?php

namespace Drupal\drd;

/**
 * Class LibraryBuild.
 *
 * @package Drupal\drd
 */
class LibraryBuild {

  // Keep this for compatibility reasons.
  const DRD_LIBRARY_VERSION = '1.7.0';

}

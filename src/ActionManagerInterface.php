<?php


namespace Drupal\drd;


use Drupal\drd\Entity\BaseInterface as RemoteEntityInterface;
use Drupal\drd\Plugin\Action\BaseInterface;
use Drupal\taxonomy\Plugin\views\wizard\TaxonomyTerm;

interface ActionManagerInterface {

  /**
   * Create an action instance for the given action ID.
   *
   * @param string $id
   *   The action id.
   *
   * @return BaseInterface|bool
   *   Returns the action instance or FALSE if no action with the given id was
   *   found or if it had the wrong type.
   */
  public function instance($id);

  /**
   * Create and action instance and execute it on a given remote entity.
   *
   * @param string $id
   *   The action id.
   * @param \Drupal\drd\Entity\BaseInterface $remote
   *   The remote DRD entity.
   * @param array $arguments
   *   The action arguments.
   *
   * @return array|bool|string
   *   The json decoded response from the remote entity or FALSE, if execution
   *   failed.
   */
  public function response($id, RemoteEntityInterface $remote, array $arguments = []);

  /**
   * @param TaxonomyTerm|string $term
   *
   * @return \Drupal\drd\Plugin\Action\BaseInterface[]
   *   All action plugins depending on mode and/or term.
   */
  public function getActionsByTerm($term): array;

}

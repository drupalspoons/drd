<?php

namespace Drupal\drd;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Action\ActionManager as CoreActionManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\drd\Entity\BaseInterface as RemoteEntityInterface;
use Drupal\drd\Plugin\Action\BaseEntityRemote;
use Drupal\drd\Plugin\Action\BaseInterface;
use Drupal\system\ActionConfigEntityInterface;
use Traversable;

class ActionManager extends CoreActionManager implements ActionManagerInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @inheritDoc
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($namespaces, $cache_backend, $module_handler);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function instance($id) {
    /** @var BaseEntityRemote $action */
    try {
      $action = $this->createInstance($id);
    } catch (PluginException $e) {
      return FALSE;
    }
    if ($action === NULL || !($action instanceof BaseInterface)) {
      return FALSE;
    }
    return $action;
  }

  /**
   * {@inheritdoc}
   */
  public function response($id, RemoteEntityInterface $remote, array $arguments = []) {
    /* @var \Drupal\drd\Plugin\Action\BaseEntityInterface $action */
    if ($action = $this->instance($id)) {
      foreach ($arguments as $key => $value) {
        $action->setActionArgument($key, $value);
      }
      return $action->executeAction($remote);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getActionsByTerm($term): array {
    $actions = [];
    $terms = [];
    try {
      $terms = is_string($term) ?
        $this->entityTypeManager->getStorage('taxonomy_term')
          ->loadByProperties(['name' => $term]) :
        [$term];
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }
    if ($terms) {
      $term = reset($terms);
      try {
        $actions = array_filter($this->entityTypeManager->getStorage('taxonomy_term')
          ->loadMultiple(), static function (ActionConfigEntityInterface $action) use ($term) {
          $plugin = $action->getPlugin();
          return ($plugin instanceof BaseInterface) && $plugin->hasTerm($term) && $plugin->access(NULL);
        });
      } catch (InvalidPluginDefinitionException $e) {
      } catch (PluginNotFoundException $e) {
      }
    }
    return $actions;
  }

}

<?php

namespace Drupal\drd\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drd\Widgets;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for DRD Info routes.
 */
class Dashboard extends ControllerBase {

  /**
   * @var \Drupal\drd\Widgets
   */
  protected $widgets;

  /**
   * Dashboard constructor.
   *
   * @param \Drupal\drd\Widgets $widgets
   */
  public function __construct(Widgets $widgets) {
    $this->widgets = $widgets;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drd.widgets')
    );
  }

  /**
   * Displays DRD status report.
   *
   * @return array
   *   A render array.
   */
  public function status(): array {
    return [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['drd-dashboard'],
        ],
      ] + $this->widgets->findWidgets(TRUE);
  }

}

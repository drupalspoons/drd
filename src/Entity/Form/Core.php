<?php

namespace Drupal\drd\Entity\Form;

use Drupal;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drd\Crypt\Base as CryptBase;
use Drupal\drd\Crypt\BaseMethodInterface;
use Drupal\drd\Entity\Domain as DomainEntity;
use Drupal\drd\Plugin\Auth\BaseInterface;
use Drupal\drd\Plugin\Auth\Manager;
use Drupal\drd\Update\ManagerStorageInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Form controller for Core edit forms.
 *
 * @ingroup drd
 */
class Core extends ContentEntityForm {

  /**
   * @var \Drupal\drd\Plugin\Auth\Manager
   */
  protected $authManager;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\drd\Update\ManagerStorageInterface
   */
  protected $managerStorage;

  /**
   * Core constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\drd\Plugin\Auth\Manager $auth_manager
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\drd\Update\ManagerStorageInterface $manager_storage
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, Manager $auth_manager, Request $request, ManagerStorageInterface $manager_storage) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->authManager = $auth_manager;
    $this->request = $request;
    $this->managerStorage = $manager_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.drd_auth'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('plugin.manager.drd_update.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    /* @var \Drupal\drd\Entity\CoreInterface $core */
    $core = $this->entity;

    if ($core->isNew()) {
      // We are coming from a specific host's Add Core action - pre-select it.
      $form['host']['widget']['#default_value'] = [$this->request->get('drd_host') ?: 1];

      // Adding a new core means we need the URL to initially contact that site
      // to grab all the details about that Drupal installation.
      $form['drd-new-core-wrapper'] = [
        '#type' => 'container',
        '#weight' => -99,
      ];
      $form['drd-new-core-wrapper']['url'] = [
        '#title' => $this->t('URL'),
        '#type' => 'url',
        '#default_value' => '',
        '#description' => $this->t('Provide the URL including scheme (e.g. https://www.example.com) and then press the TAB key (or leave the field otherwise) so that DRD will validate the URL and provide you with more setting fields.'),
        '#required' => TRUE,
        '#ajax' => [
          'callback' => [$this, 'validateUrlAjax'],
          'event' => 'change',
          'progress' => [
            'type' => 'throbber',
            'message' => t('Verifying url...'),
          ],
        ],
      ];
      $form['drd-new-core-wrapper']['url-message'] = [
        '#type' => 'container',
      ];

      // Container for domain specific settings.
      $form['drd-new-core-wrapper']['drd'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => 'hidden',
        ],
      ];
      $form['drd-new-core-wrapper']['drd']['drd_auth'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Authentication type'),
      ];
      $form['drd-new-core-wrapper']['drd']['drd_auth']['description'] = [
        '#markup' => $this->t('The method how DRD should authenticate each request on the remote domains on this core.'),
      ];
      $form['drd-new-core-wrapper']['drd']['drd_auth']['drd_auth_type'] = [
        '#type' => 'select',
        '#options' => $this->authManager->selectList(),
        '#default_value' => 'shared_secret',
      ];
      foreach ($this->authManager->getDefinitions() as $def) {
        /** @var BaseInterface $auth */
        try {
          $auth = $this->authManager->createInstance($def['id']);
        } catch (PluginException $e) {
          continue;
        }
        $condition = ['select#edit-drd-auth-type' => ['value' => $def['id']]];
        $form['drd-new-core-wrapper']['drd']['drd_auth'][$def['id']] = [
          '#type' => 'container',
          '#states' => [
            'visible' => $condition,
          ],
        ];
        $auth->settingsForm($form['drd-new-core-wrapper']['drd']['drd_auth'][$def['id']], $condition);
      }

      $form['drd-new-core-wrapper']['drd']['drd_crypt'] = [
        '#type' => 'fieldset',
        '#title' => t('Encryption type'),
      ];
      $form['drd-new-core-wrapper']['drd']['drd_crypt']['description'] = [
        '#markup' => t('The method how DRD should encrypt the data sent to and received from the remote domains on this core.'),
      ];
      $form['drd-new-core-wrapper']['drd']['drd_crypt']['drd_crypt_type'] = [
        '#type' => 'select',
        '#default_value' => 'OpenSSL',
      ];
      $options = [];
      /* @var string $key */
      /* @var BaseMethodInterface $method */
      foreach (CryptBase::getMethods(TRUE) as $key => $method) {
        $options[$key] = $key;
        $condition = ['select#edit-drd-crypt-type' => ['value' => $key]];
        $form['drd-new-core-wrapper']['drd']['drd_crypt'][$key] = [
          '#type' => 'container',
          '#states' => [
            'visible' => $condition,
          ],
        ];
        $method->settingsForm($form['drd-new-core-wrapper']['drd']['drd_crypt'][$key], $condition);
      }
      $form['drd-new-core-wrapper']['drd']['drd_crypt']['drd_crypt_type']['#options'] = $options;

      // Hide the actions until the domain specific settings will be displayed.
      $form['actions']['#attributes']['class'][] = 'hidden';
    }
    else {
      $form['host']['#disabled'] = TRUE;
    }

    $this->managerStorage->buildGlobalForm($form, $form_state, $core->getUpdateSettings());

    return $form;
  }

  /**
   * Validates that the url field points to a drd_agent enabled domain.
   *
   * @param array $form
   *   Form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return bool|string|array
   *   If the URI is not provided yet, return FALSE. If we can't receive a list
   *   of crypt methods from remote, return a message as a string explaining the
   *   reason. If all goes allright, we return an array with the list of
   *   available crypt methods.
   */
  private function validateUrl(array &$form, FormStateInterface $form_state) {
    $uri = trim($form_state->getValue('url'), ' /');
    if (empty($uri)) {
      return FALSE;
    }

    $client = Drupal::service('http_client_factory')->fromOptions();
    $response = $client->head($uri, ['allow_redirects' => FALSE]);
    $status_code = $response->getStatusCode();
    if ($status_code === 301) {
      $uri = $response->getHeaderLine('location');
    }
    elseif ($status_code > 301) {
      return FALSE;
    }

    /* @var \Drupal\drd\Entity\CoreInterface $core */
    $core = $this->entity;

    $values = [
      'auth' => $form_state->getValue('drd_auth_type'),
      'authsetting' => [],
      'crypt' => $form_state->getValue('drd_crypt_type'),
      'cryptsetting' => [],
    ];
    foreach ($this->authManager->getDefinitions() as $def) {
      /** @var BaseInterface $auth */
      try {
        $auth = $this->authManager->createInstance($def['id']);
      } catch (PluginException $e) {
        // Can be ignored, we checked for types right before.
      }
      $values['authsetting'][$def['id']] = $auth->settingsFormValues($form_state);
    }
    foreach (CryptBase::getMethods(TRUE) as $key => $method) {
      $values['cryptsetting'][$key] = $method->settingsFormValues($form_state);
    }

    try {
      $domain = DomainEntity::instanceFromUrl($core, $uri, $values);
    } catch (Exception $e) {
      return FALSE;
    }
    if ($domain === NULL) {
      return FALSE;
    }

    if (!$domain->isNew()) {
      return $this->t('This domain is already known to the dashboard.')
        ->render();
    }

    $crypt_methods = $domain->getSupportedCryptMethods();
    if ($crypt_methods === FALSE) {
      return $this->t('Can not connect to this domain.')->render();
    }
    if (empty($crypt_methods)) {
      return $this->t('There is no DRD Agent available at this domain.')
        ->render();
    }
    if (CryptBase::countAvailableMethods($crypt_methods) === 0) {
      return $this->t('The remote site has DRD Agent installed but does not support any encryption methods matching those of the dashboard.')
        ->render();
    }

    $form_state->setTemporaryValue('drd_domain', $domain);
    return $crypt_methods;
  }

  /**
   * Ajax callback for checking remote domain.
   *
   * Ajax callback to lookup a remote domain and receive their supported crypt
   * methods which will be integrated into the settings form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Response object with instructions on how to adjust the form.
   */
  public function validateUrlAjax(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();
    $crypt_methods = $this->validateUrl($form, $form_state);
    if (is_array($crypt_methods)) {
      $command = 'removeClass';
      $message = '';
      foreach (CryptBase::getMethods() as $key => $value) {
        $response->addCommand(new InvokeCommand('#edit-drd-crypt-type option[value="' . $key . '"]', 'prop', [
          'disabled',
          !isset($crypt_methods[$key]),
        ]));
      }
    }
    else {
      $command = 'addClass';
      $message = $crypt_methods;
    }
    $response->addCommand(new InvokeCommand('#edit-drd', $command, ['hidden']));
    $response->addCommand(new InvokeCommand('#edit-actions', $command, ['hidden']));
    $response->addCommand(new HtmlCommand('#edit-url-message', $message));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\drd\Entity\CoreInterface $core */
    $core = parent::validateForm($form, $form_state);
    if (!$form_state->hasAnyErrors() && $core->isNew()) {
      $error = $this->validateUrl($form, $form_state);
      if (!empty($error) && is_string($error)) {
        $form_state->setErrorByName('url', $error);
      }
    }
    $this->managerStorage->validateGlobalForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\drd\Entity\CoreInterface $core */
    $core = $this->entity;

    $core->set('updsettings', $this->managerStorage->globalFormValues($form, $form_state));

    $status = $core->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('Created the %label core.', [
        '%label' => $core->label(),
      ]));

      /** @var \Drupal\drd\Entity\DomainInterface $domain */
      $domain = $form_state->getTemporaryValue('drd_domain');
      $domain->setCore($core);
      $domain->save();
      $this->messenger()
        ->addMessage($this->t('Now you should @configure your remote domain. Make sure you are logged in to the remote site before you click the link!', [
          '@configure' => $domain->getRemoteSetupLink($this->t('configure'), TRUE),
        ]));
    }
    else {
      $this->messenger()->addMessage($this->t('Saved the %label Core.', [
        '%label' => $core->label(),
      ]));
    }
    $form_state->setRedirect('entity.drd_core.canonical', ['drd_core' => $core->id()]);
  }

}
